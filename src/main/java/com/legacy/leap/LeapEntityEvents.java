package com.legacy.leap;

import com.legacy.leap.player.LeapPlayer;
import com.legacy.leap.player.util.CapabilityProvider;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class LeapEntityEvents
{
	@SubscribeEvent
	public static void onCapabilityAttached(AttachCapabilitiesEvent<Entity> event)
	{
		if (event.getObject() instanceof Player && !event.getObject().getCapability(LeapPlayer.INSTANCE).isPresent())
			event.addCapability(LeapMod.locate("player_capability"), new CapabilityProvider(new LeapPlayer((Player) event.getObject())));
	}

	@SubscribeEvent
	public static void onLivingFall(LivingFallEvent event)
	{
		if (event.getEntityLiving() instanceof Player && !event.getEntityLiving().level.isClientSide)
			LeapPlayer.ifPresent((Player) event.getEntity(), leapPlayer -> leapPlayer.onFall(event));
	}
}
