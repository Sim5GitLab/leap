package com.legacy.leap;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.KeyMapping;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.ClientRegistry;

@OnlyIn(Dist.CLIENT)
public class LeapKeyRegistry
{
	List<KeyMapping> keys;

	public LeapKeyRegistry()
	{
		this.keys = new ArrayList<KeyMapping>();
	}

	public LeapKeyRegistry register(KeyMapping keyBinding)
	{
		if (!keys.contains(keyBinding))
		{
			this.keys.add(keyBinding);
			ClientRegistry.registerKeyBinding(keyBinding);
		}
		return this;
	}
}
