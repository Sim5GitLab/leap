package com.legacy.leap;

import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantment.Rarity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = LeapMod.MODID, bus = Bus.MOD)
public class LeapRegistry
{
	public static final Enchantment LEAPING = new LeapingEnchantment(Rarity.RARE, EquipmentSlot.FEET);

	@SubscribeEvent
	public static void registerEnchantments(Register<Enchantment> event)
	{
		register(event.getRegistry(), "leaping", LEAPING);
	}

	private static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(LeapMod.locate(name));
		registry.register(object);
	}
}
